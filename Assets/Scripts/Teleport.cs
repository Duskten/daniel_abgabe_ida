﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {
	public GameObject exitpoint;

	public float delay = 0.2f;

	public bool forced;


	public void OnTriggerEnter2D(Collider2D other){

		if (!forced) {
			if (Input.GetButtonDown ("Use")) {
				StartCoroutine (TeleportTo (other));
			}
		}
		else StartCoroutine (TeleportTo (other));
	}

	IEnumerator TeleportTo(Collider2D collider){
		yield return new WaitForSeconds (1 * delay);
		if (collider != null) {
			collider.transform.position = new Vector3 (exitpoint.transform.position.x, exitpoint.transform.position.y, exitpoint.transform.position.z);
		}
	}
}
