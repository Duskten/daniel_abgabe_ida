﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float speed = 20f;
	public int damage = 20;
	public float lifespan = 5f;

	public bool destroyable = false;

	public Rigidbody2D rb;
	public GameObject impactEffect;

	void Start () {
		rb.velocity = transform.right * speed;
	}

	void Update()
	{
		lifespan -= Time.deltaTime;

		if (lifespan < 0)
		{
			Impact();
		}

		if(destroyable && Input.GetButtonDown("Fire1"))
		{
			Impact();
		}
	}

	private void OnTriggerEnter2D(Collider2D hitInfo)
	{
		Debug.Log(hitInfo.name);
		Enemy enemy = hitInfo.GetComponent<Enemy>();
		if(enemy != null)
		{
			Player player = GetComponent<Player>();
			enemy.TakeDamage(damage);
		}

		Impact();
	}

	void Impact ()
	{
		Instantiate(impactEffect, transform.position, transform.rotation);

		Destroy(gameObject);
	}

}
