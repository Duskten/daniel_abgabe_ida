﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security;

public class Npc_Animation : MonoBehaviour {
	Animator animator;
	NpcWalk npc;
	Controller2D controller;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		npc = GetComponentInParent<NpcWalk> ();
		controller = GetComponentInParent<Controller2D> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		/*
		 * 0 = Idle
		 * 1 = Walk
		 */

		if (npc.move.x != 0) {
			Debug.Log ("Player Walks");
			animator.SetInteger ("AnimationState", 1);
		} 
		else {
			animator.SetInteger ("AnimationState", 0);

		}


	}
}