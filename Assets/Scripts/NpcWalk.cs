﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcWalk : MonoBehaviour
{
	[HideInInspector]
	public Vector2 move;
	public float moveSpeed = 10;
	public bool idle;
	public LayerMask envirDetect;

	[HideInInspector]
	public BoxCollider2D collider;
	float enemyWidth;

	Transform thisEnemy;
	Rigidbody2D rb;
	GameObject target;

	public virtual void Awake()
	{
		collider = GetComponent<BoxCollider2D>();
	}

	private void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		thisEnemy = transform;
		enemyWidth = GetComponent<SpriteRenderer>().bounds.extents.x;
	}

	private void FixedUpdate()
	{
		//Horizontal Environment Detection
		Vector2 lineCastPosHor = thisEnemy.position + (thisEnemy.right * enemyWidth);
		Debug.DrawLine(lineCastPosHor, lineCastPosHor + Vector2.right);
		bool hitWall = Physics2D.Linecast(lineCastPosHor, lineCastPosHor + Vector2.right, envirDetect);

		//Vertical Environment Detection
		Vector2 lineCastPosVer = thisEnemy.position + (thisEnemy.right * enemyWidth);
		Debug.DrawLine(lineCastPosVer, lineCastPosVer + Vector2.down);
		bool isGrounded = Physics2D.Linecast(lineCastPosVer, lineCastPosVer + Vector2.down, envirDetect);

		//EnvirReaction
		if (!isGrounded || hitWall)
		{
			Vector2 currentRotation = thisEnemy.eulerAngles;
			currentRotation.y += 180;
			thisEnemy.eulerAngles = currentRotation;
		}
		if (!idle) {
			move = rb.velocity;
			move.x = thisEnemy.right.x * moveSpeed;
			rb.velocity = move;
		}
	}
}