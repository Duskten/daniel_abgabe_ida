﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blaster : MonoBehaviour
{

	public Transform firePoint;

	public GameObject prefabProjectile;
	public float timeToFire = 1f;
	float fireRate;
	public bool canShoot = true;
	public bool cloneMachine;

	Player player;

	private void Start()
	{

		Debug.Log ("BlasterStart");
		fireRate = timeToFire;
	}

	void Update()
	{

		player = GetComponent<Player>();
		fireRate -= Time.deltaTime;

		if (fireRate <= 0)
		{
			canShoot = true;
		}
			

//		if (canShoot)
//		{
			if (Input.GetButtonDown ("Fire1")) {
				Debug.Log ("FireInput");
				Shoot (firePoint);

			} else if (cloneMachine) {
				Shoot (firePoint);
			}	


//				canShoot = false;
//				fireRate = timeToFire;
//				Debug.Log("FireWeapon");
//			}
//			else
//			{
//				return;
//			}
		}





	void Shoot (Transform transform)
	{
		Debug.Log ("Shoot Bullet");
		Instantiate(prefabProjectile, transform.position, transform.rotation);

	}

}