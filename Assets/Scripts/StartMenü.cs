﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.CompilerServices;

public class StartMenü : MonoBehaviour {

	public void StartGame ()
	{
		SceneManager.LoadScene("Level1");
	}

	public void QuitGame()
	{
		Debug.Log ("Spiel ist geschlossen");
		Application.Quit ();
	}
}
