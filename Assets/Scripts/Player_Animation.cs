﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security;

public class Player_Animation : MonoBehaviour {
	Animator animator;
	Player player;
	Controller2D controller;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		player = GetComponentInParent<Player> ();
		controller = GetComponentInParent<Controller2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		/*
		 * 0 = Idle
		 * 1 = Walk
		 */

		if (controller.collisions.below && player.directionalInput.x != 0) {
				Debug.Log ("Player Walks");
				animator.SetInteger ("AnimationState", 1);
			} 
		else {
			animator.SetInteger ("AnimationState", 0);

		}


	}
}